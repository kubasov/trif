var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});
router.get('/light', function(req, res, next) {
  res.render('light');
});
router.get('/materials', function(req, res, next) {
  res.render('materials');
});
router.get('/mebel', function(req, res, next) {
  res.render('mebel');
});
router.get('/building', function(req, res, next) {
  res.render('building');
});
router.get('/contacts', function(req, res, next) {
  res.render('contacts');
});
router.get('/land-catalog', function(req, res, next) {
  res.render('light-land-catalog');
});
module.exports = router;
